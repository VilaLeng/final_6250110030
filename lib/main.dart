import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Remove the debug banner
      debugShowCheckedModeBanner: false,
      title: 'Final Test: Firebase CRUD',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  //const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // text fields' controllers
  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();
  final TextEditingController _majorController = TextEditingController();
  final TextEditingController _studentIdController = TextEditingController();

  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _students =
  FirebaseFirestore.instance.collection('students');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _studentIdController.text = documentSnapshot['student_id'].toString();
      _firstnameController.text = documentSnapshot['firstname'];
      _lastnameController.text = documentSnapshot['lastname'];
      _majorController.text = documentSnapshot['major'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                  controller: _studentIdController,
                  decoration: const InputDecoration(
                    labelText: 'Student ID',
                  ),
                ),
                TextField(
                  controller: _firstnameController,
                  decoration: const InputDecoration(labelText: 'Firstname'),
                ),
                TextField(
                  controller: _lastnameController,
                  decoration: const InputDecoration(labelText: 'Lastname'),
                ),
                TextField(
                  controller: _majorController,
                  decoration: const InputDecoration(labelText: 'Major'),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final double student_id =
                    double.tryParse(_studentIdController.text);
                    final String firstname = _firstnameController.text;
                    final String lastname = _lastnameController.text;
                    final String major = _majorController.text;
                    if (student_id != null &&
                        firstname != null &&
                        lastname != null &&
                        major != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _students
                            .add({
                          "student_id": student_id,
                          "firstname": firstname,
                          "lastname": lastname,
                          "major": major
                        })
                            .then((value) => print("Students Added"))
                            .catchError((error) =>
                            print("Failed to add students: $error"));
                      }

                      if (action == 'update') {
                        // Update the product
                        await _students
                            .doc(documentSnapshot?.id)
                            .update({
                          "student_id": student_id,
                          "firstname": firstname,
                          "lastname": lastname,
                          "major": major,
                        })
                            .then((value) => print("Student Updated"))
                            .catchError((error) =>
                            print("Failed to update students: $error"));
                      }

                      // Clear the text fields
                      _studentIdController.text = '';
                      _firstnameController.text = '';
                      _lastnameController.text = '';
                      _majorController.text ='';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteStudent(String studentId) async {
    await _students
        .doc(studentId)
        .delete()
        .then((value) => print("Student Deleted"))
        .catchError((error) => print("Failed to delete product: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a student')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Final Test: Firebase CRUD'),
      ),
      // Using StreamBuilder to display all products from Firestore in real-time
      body: StreamBuilder(
        stream: _students.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data?.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                streamSnapshot.data.docs[index];
                return Card(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['student_id'].toString()),
                    subtitle: Text(documentSnapshot['firstname'].toString()+' '+documentSnapshot['lastname'].toString()),
                    leading: Icon(Icons.account_circle, size: 45,),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [

                          // Press this button to edit a single product
                          IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete a single product
                          IconButton(
                            icon: const Icon(Icons.delete),
                            /*onPressed: () =>
                                  _deleteStudent(documentSnapshot.id)*/
                            onPressed: (){
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    backgroundColor: Colors.white,
                                    title: new Text(
                                      "Please Confirm",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 23,
                                      ),
                                    ),
                                    content: Text('Are you sure to remove student?'),
                                    // content: new Text("Please Confirm"),
                                    actions: [
                                      new TextButton(
                                        onPressed: () {
                                          _deleteStudent(documentSnapshot.id);
                                          setState(() {
                                            Navigator.of(context).pop();
                                          });
                                        },
                                        /* onPressed: () =>
                                       _deleteProduct(documentSnapshot.id),*/
                                        child: new Text("Yes",style: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 23,
                                        ),),
                                      ),
                                      Visibility(
                                        visible: true,
                                        child: new TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: new Text("No",style: TextStyle(
                                            color: Colors.blue,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 23,
                                          ),),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      // Add new product
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
